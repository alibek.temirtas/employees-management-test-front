import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { rootEffects } from './effects';
import { rootReducers } from './reducers';
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../../environments/environment';

export const metaReducers: MetaReducer<any>[] = !environment.production ? [storeFreeze] : [];

@NgModule({
  imports: [
    StoreModule.forRoot(rootReducers, { metaReducers }),
    EffectsModule.forRoot(rootEffects),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
    })
  ],
  declarations: []
})
export class RootStoreModule {}
