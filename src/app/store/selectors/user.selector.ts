import { createSelector, createFeatureSelector } from '@ngrx/store';
import { UserState } from '../states';
import { User } from '../../shared/models';

export const USER_NODE = 'user';

export const selectUserState = createFeatureSelector<UserState>(USER_NODE);

export const selectUser = createSelector(
  selectUserState,
  (state: UserState) => state.user
);

export const selectLoginError = createSelector(
  selectUserState,
  (state: UserState) => state.loginError
);
