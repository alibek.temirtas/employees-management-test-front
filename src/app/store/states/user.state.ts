import { User } from '../../shared/models';

export interface UserState {
  user: User;
  loginError: string;
}

export const initialUserState: UserState = {
  user: JSON.parse(localStorage.getItem('user')) || null,
  loginError: ''
};
