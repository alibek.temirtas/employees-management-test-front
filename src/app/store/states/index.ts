import { UserState } from './user.state';

import { USER_NODE } from '../selectors';

export interface RootState {
  [USER_NODE]: UserState
}

export * from './user.state';
