import { ActionReducerMap } from '@ngrx/store';
import { userReducer } from './user.reducer';
import { RootState } from '../states';

export const rootReducers: ActionReducerMap<RootState> = {
  user: userReducer
};
