import { initialUserState, UserState } from '../states';
import { UserActions, UserActionTypes } from '../actions';

export function userReducer(state = initialUserState, action: UserActions): UserState {
    switch (action.type) {
        case UserActionTypes.SET_USER_DATA_SUCCESS: {
            return {
                ...state,
                user: action.payload.user
            };
        }
        case UserActionTypes.LOGOUT_SUCCESS: {
            return {
                ...state,
                user: null,
                loginError: ''
            };
        }
        case UserActionTypes.LOGIN_ERROR: {
            let loginError: string;
            if (action.payload.status === 401) {
                loginError = 'Не правильный логин или пароль';
            }
            return {
                ...state,
                loginError: loginError || 'Ошибка сервера'
            };
        }
        default: {
            return state;
        }
    }
}
