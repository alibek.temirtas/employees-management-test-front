import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { AuthService } from '../../auth/services/auth.service';
import { Router } from '@angular/router';
import { UserActionTypes, Login, SetUserData, ErrorAction, LogoutSuccess, SetUserDataSuccess, Logout } from '../actions';
import { routesNames } from '../../shared/enums/routes-names';

@Injectable()
export class UserEffects {

    @Effect()
    login$: Observable<any> = this.actions$.pipe(
        ofType(UserActionTypes.LOGIN),
        map((action: Login) => action.payload.authData),
        switchMap(authData => {
            return this.authService.login(authData).pipe(
              map((data) => {
                return this.saveToken(data);
              }),
                catchError(err => {
                return of(new ErrorAction({errors: err}));
              }));
        })
    );

    @Effect()
    setUserData$: Observable<any> = this.actions$.pipe(
        ofType(UserActionTypes.SET_USER_DATA),
        map((action: SetUserData) => action.payload.redirect),
        switchMap((redirect) => {
            return this.authService.getUserDetails().pipe(map(user => {
                localStorage.setItem('user', JSON.stringify(user));
                if (redirect) {
                    this.router.navigateByUrl('/employee');
                }
                return new SetUserDataSuccess({ user });
            }),catchError(err => {
                return of(new ErrorAction({errors: err}));
              }));
        })
    );

    @Effect()
    logout$: Observable<any> = this.actions$.pipe(
        ofType(UserActionTypes.LOGOUT),
        map((action: Logout) => action.payload),
        switchMap(_ => {
            return of([]).pipe(map(() => {
                return this.logoutHandle();
            }));
        })
    );

    constructor(private actions$: Actions, private authService: AuthService, private router: Router) { }

    private logoutHandle() {
        localStorage.removeItem('access_token');
        localStorage.removeItem('user');
        this.router.navigateByUrl(routesNames.AUTH, { state: { skipRequest: true } });
        return new LogoutSuccess();
    }

    private saveToken(data) {
        localStorage.setItem('access_token', data.accessToken);
        return new SetUserData({ redirect: true });
      }
}
