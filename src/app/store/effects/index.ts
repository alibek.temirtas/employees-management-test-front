import { UserEffects } from './user.effects';
import {ErrorEffects} from './error.effects';

export const rootEffects = [UserEffects, ErrorEffects];
