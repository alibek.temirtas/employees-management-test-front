import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { ErrorActionTypes, ErrorAction} from '../actions';

import {MiddlewareResponseService} from '../../shared/services/middleware-response.service';

@Injectable()
export class ErrorEffects {

  @Effect({
    dispatch: false
  })
  errorEffect$: Observable<any> = this.actions$.pipe(
    ofType(ErrorActionTypes.ERROR),
    map((action: ErrorAction) =>
      this._middleWareResponseSerivce.handleError(action.payload.errors))
  );

  constructor(private actions$: Actions, private _middleWareResponseSerivce: MiddlewareResponseService) { }

}
