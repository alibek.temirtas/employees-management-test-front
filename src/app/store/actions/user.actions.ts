import { Action } from '@ngrx/store';
import { User } from '../../shared/models';
import { AuthData } from '../../auth/models/auth-data.model';
import { HttpErrorResponse } from '@angular/common/http';


export enum UserActionTypes {
  LOGIN = '[User] Login',
  LOGIN_ERROR = '[User] Login error',
  SET_USER_DATA = '[User] Set user data',
  SET_USER_DATA_SUCCESS = '[User] Set user data success',
  LOGOUT = '[User] Logout',
  LOGOUT_SUCCESS = '[User] Logout success',
  OBTAIN_TOKEN = '[User] Obtain token'
}

export class Login implements Action {
  readonly type = UserActionTypes.LOGIN;
  constructor(public payload: { authData: AuthData }) {}
}

export class LoginError implements Action {
  readonly type = UserActionTypes.LOGIN_ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export class SetUserData implements Action {
  readonly type = UserActionTypes.SET_USER_DATA;
  constructor(public payload: {redirect: boolean} = {redirect: false}) {}
}

export class SetUserDataSuccess implements Action {
  readonly type = UserActionTypes.SET_USER_DATA_SUCCESS;
  constructor(public payload: { user: User}) {}
}

export class Logout implements Action {
  readonly type = UserActionTypes.LOGOUT;
  constructor(public payload: { sendRequest: boolean } = { sendRequest: false }) {}
}

export class LogoutSuccess implements Action {
  readonly type = UserActionTypes.LOGOUT_SUCCESS;
}

export type UserActions =
  | Login
  | LoginError
  | SetUserData
  | SetUserDataSuccess
  | Logout
  | LogoutSuccess;
