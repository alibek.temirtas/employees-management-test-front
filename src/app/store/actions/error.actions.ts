import { Action } from '@ngrx/store';


export enum ErrorActionTypes {
  ERROR = '[ERROR] new error',
}
export class ErrorAction implements Action {
  readonly type = ErrorActionTypes.ERROR;
  constructor(public payload: { errors: any }) {}
}

export type ErrorActionsType =
  | ErrorAction;
