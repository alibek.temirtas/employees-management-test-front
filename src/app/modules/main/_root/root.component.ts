import { Component } from '@angular/core';

@Component({
  selector: 'app-main-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss']
})
export class MainRootComponent {
}
