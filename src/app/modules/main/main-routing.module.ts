import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { routesNames } from '../../shared/enums/routes-names';

// COMPONENTS :
import {MainRootComponent} from './_root/root.component';
import {MainEmployeeComponent} from './employee/employee.component';
import {MainCompanyComponent} from './company/company.component';
// TODO : When add several roles of user (ADMIN, OPER), should change structur. And of course need to use lazy loading.

const routes: Routes = [
  {
    path: '',
    component: MainRootComponent,
    children: [
      {
        path: routesNames.EMPLOYEE,
        component: MainEmployeeComponent
      },
      {
        path: routesNames.COMPANY,
        component: MainCompanyComponent
      },
      {
        path: '',
        redirectTo: routesNames.EMPLOYEE
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
