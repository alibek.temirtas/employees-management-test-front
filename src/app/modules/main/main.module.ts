import { NgModule } from '@angular/core';

import { MainRoutingModule } from './main-routing.module';
import { SharedModule } from '../../shared/shared.module';

import {MainRootComponent} from './_root/root.component';
import {MainEmployeeComponent} from './employee/employee.component';
import {MainCompanyComponent} from './company/company.component';

import {CreateCompanyModalComponent} from './modals/company/create-company/create-company.component';
import {CreateEmployeeModalComponent} from './modals/employee/create-employee/create-employee.component';

import {CompanyService} from './services/company.service';
import {EmployeeService} from './services/employee.service';

@NgModule({
  declarations: [
    MainRootComponent,
    MainEmployeeComponent,
    MainCompanyComponent,
    CreateCompanyModalComponent,
    CreateEmployeeModalComponent
  ],
  imports: [
    MainRoutingModule,
    SharedModule
  ],
  providers: [
    CompanyService,
    EmployeeService
  ]
})
export class MainModule { }
