import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';

import {IPaginationResult, ICompany, IResponseData, IAuthenticationResult} from '../../../shared/models'

import {MiddlewareResponseService} from '../../../shared/services/middleware-response.service';

@Injectable()
export class CompanyService {
    constructor(private http: HttpClient, private _middleWareResponseSerivce: MiddlewareResponseService) { }

    createCompany(formData: {name: string, description?: string}) {
        return this.http.post(`company`, formData).pipe(
            map((response: IResponseData<IAuthenticationResult>) => {
              return this._middleWareResponseSerivce.parseResponse<IAuthenticationResult>(response);
            })
          );
    }

    getCompanyList(): Observable<IPaginationResult<ICompany[]>> {
        return this.http.get(`company/?page=1&perpage=100`).pipe(
            map((response: IResponseData<IPaginationResult<ICompany[]>>) => {
              return this._middleWareResponseSerivce.parseResponse<IPaginationResult<ICompany[]>>(response);
            })
          );
    }
    
    updateCompany(formData: {name: string}): Observable<any>  {
      return this.http.put('company', formData).pipe(
        map((response: IResponseData<any>) => {
          return this._middleWareResponseSerivce.parseResponse<any>(response);
        })
      );
    }

    deleteCompany(formData: {companyId: string}): Observable<any> {
      return this.http.delete(`company`, {params: formData}).pipe(
        map((response: any) => {
          return this._middleWareResponseSerivce.parseResponse<null>(response);
        }),
      );
    }

}
