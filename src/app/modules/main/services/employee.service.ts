import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';

import {IPaginationResult, User, IResponseData, IAuthenticationResult} from '../../../shared/models'

import {MiddlewareResponseService} from '../../../shared/services/middleware-response.service';

@Injectable()
export class EmployeeService {
    constructor(private http: HttpClient, private _middleWareResponseSerivce: MiddlewareResponseService) { }

    createEmployee(formData: {name: string, description?: string}) {
        return this.http.post(`employee`, formData).pipe(
            map((response: IResponseData<IAuthenticationResult>) => {
              return this._middleWareResponseSerivce.parseResponse<IAuthenticationResult>(response);
            })
          );
    }

    getEmployeeList(paramData): Observable<IPaginationResult<User[]>> {
        return this.http.get(`employee/list/?page=${paramData.page}&perpage=${paramData.pageSize}${paramData.companyId ? '&companyId='+paramData.companyId : ''}`).pipe(
            map((response: IResponseData<IPaginationResult<User[]>>) => {
              return this._middleWareResponseSerivce.parseResponse<IPaginationResult<User[]>>(response);
            })
          );
    }
    
    updateEmployee(formData): Observable<any>  {
      return this.http.put('employee', formData).pipe(
        map((response: IResponseData<any>) => {
          return this._middleWareResponseSerivce.parseResponse<any>(response);
        })
      );
    }

    deleteEmployee(formData: {userId: string}): Observable<any> {
      return this.http.delete(`employee`, {params: formData}).pipe(
        map((response: any) => {
          return this._middleWareResponseSerivce.parseResponse<null>(response);
        }),
      );
    }

}
