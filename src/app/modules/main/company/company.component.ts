import { Component, OnInit, OnDestroy } from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {CreateCompanyModalComponent} from '../modals/company/create-company/create-company.component';

import {CompanyService} from '../services/company.service';

import {ICompany} from '../../../shared/models';

@Component({
  selector: 'app-main-company',
  templateUrl: './company.component.html'
})
export class MainCompanyComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['name', 'edit', 'delete'];
  dataSource: ICompany[] = [];

  private _onDestroy = new Subject();

  constructor(private _companyService: CompanyService,
              private _snackBar: MatSnackBar,
              public dialog: MatDialog){}

  ngOnInit() {

    this.getData();

  }

  ngOnDestroy(){
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  getData(){
    this._companyService.getCompanyList().pipe(takeUntil(this._onDestroy)).subscribe((data)=>{
      this.dataSource = data.items;
    });
  }

  openDialog(company: ICompany = null): void {
    const dialogRef = this.dialog.open(CreateCompanyModalComponent, {
      width: '450px',
      data: company
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }
  
  deleteCompany(company){
    this._companyService.deleteCompany({companyId: company._id}).pipe(takeUntil(this._onDestroy)).subscribe((data)=>{
      this._snackBar.open('Company deleted', '', {
        duration: 3000
      });
      this.getData();
    }, err=>{
      this._snackBar.open(err, '', {
        duration: 3000
      });
    })
  }

}
