import { Component } from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {CreateEmployeeModalComponent} from '../modals/employee/create-employee/create-employee.component';
import {CompanyService} from '../services/company.service';
import {EmployeeService} from '../services/employee.service';

import {ICompany, User, IPager} from '../../../shared/models';

@Component({
  selector: 'app-main-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class MainEmployeeComponent {
  displayedColumns: string[] = ['position', 'name', 'date', 'company', 'edit', 'delete'];
  dataSource;

  companies: ICompany[] = [];
  selectedCompanyId: string;

  pager: IPager = {
    page: 1,
    pageSize: 10,
  };

  private _onDestroy = new Subject();

  constructor(private _companyService: CompanyService,
              private _employeeService: EmployeeService,
              private _snackBar: MatSnackBar,
              public dialog: MatDialog){}


  ngOnInit() {

    this.getCompanies();
    this.getEmployees();

  }

  ngOnDestroy(){
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  getCompanies(){
    this._companyService.getCompanyList().pipe(takeUntil(this._onDestroy)).subscribe((data)=>{
      this.companies = data.items;
    });
  }

  getEmployees(){
    this._employeeService.getEmployeeList({
      companyId: this.selectedCompanyId,
      ...this.pager
    }).pipe(takeUntil(this._onDestroy)).subscribe((data)=>{
      this.dataSource = data.items;

      this.pager = {
        page: data.pageIndex,
        pageSize: data.pageSize,
        totalCount: data.totalCount
      };

    });
  }

  openDialog(user: User = null): void {
    if(!this.selectedCompanyId && !user){
      this._snackBar.open('Please, choose company', '', {
        duration: 3000
      });

      return;
    }
    const dialogRef = this.dialog.open(CreateEmployeeModalComponent, {
      width: '450px',
      data: {
        companies: this.companies,
        companyId: this.selectedCompanyId,
        ...user
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getEmployees();
    });
  }
  
  deleteEmployee(user){
    this._employeeService.deleteEmployee({userId: user._id}).pipe(takeUntil(this._onDestroy)).subscribe((data)=>{
      this._snackBar.open('Employee deleted', '', {
        duration: 3000
      });
      this.getEmployees();
    }, err=>{
      this._snackBar.open(err, '', {
        duration: 3000
      });
    })
  }

  setPage(page){
    this.pager.page = page;
    this.getEmployees();
  }

}
