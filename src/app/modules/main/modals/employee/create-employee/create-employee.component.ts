import {Component, Inject, OnInit, OnDestroy} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

import {EmployeeService} from '../../../services/employee.service';

@Component({
  selector: 'app-main-create-employee',
  templateUrl: './create-employee.component.html'
})
export class CreateEmployeeModalComponent implements OnInit, OnDestroy  {
  formUser: FormGroup;
  companies = []

  private _onDestroy = new Subject();

  constructor(
    public dialogRef: MatDialogRef<any>,
    private fb: FormBuilder,
    private _employeeService: EmployeeService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

    ngOnInit(){
      this.companies = this.data.companies;
      this.formUser = this.initForm();
    }

    ngOnDestroy(){
      this._onDestroy.next();
      this._onDestroy.complete();
    }

    initForm(): FormGroup {
        return this.fb.group({
          firstName: [this.data && this.data.firstName || '', [Validators.required]],
          lastName: [this.data && this.data.lastName || '', [Validators.required]],
          email: [this.data && this.data.email || '', [Validators.email, Validators.required]],
          password: [this.data && this.data.password || '', [Validators.required]],
          companyId: [this.data && this.data.company && this.data.company._id || this.data.companyId || '', [Validators.required]]
        });
    }

    onNoClick(): void {

        const controls = this.formUser.controls;

        if(this.data && this.data._id){

          this._employeeService.updateEmployee({

            userId: this.data._id,
            ...this.formUser.value

          }).pipe(takeUntil(this._onDestroy)).subscribe((data)=>{

            this._snackBar.open('Success', '', {
              duration: 3000
            });

            this.dialogRef.close();
          }, 
          err => {
            this._snackBar.open(err, '', {
              duration: 3000
            });

          });

        } else {

          if (this.formUser.invalid) {
            Object.keys(controls)
                  .forEach(controlName => controls[controlName].markAsTouched());
              return;
          };

          this._employeeService.createEmployee(this.formUser.value).pipe(takeUntil(this._onDestroy)).subscribe((data)=>{
            
            this._snackBar.open('Success', '', {
              duration: 3000
            });

            this.dialogRef.close();

          }, err=>{

            this._snackBar.open(err, '', {
              duration: 3000
            });
            
          });

        }

        

    }
}
