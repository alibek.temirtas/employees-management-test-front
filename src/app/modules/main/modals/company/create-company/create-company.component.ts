import {Component, Inject, OnInit, OnDestroy} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

import {ICompany} from '../../../../../shared/models';
import {CompanyService} from '../../../services/company.service';

@Component({
  selector: 'app-main-create-company',
  templateUrl: './create-company.component.html'
})
export class CreateCompanyModalComponent implements OnInit, OnDestroy  {
  formCompany: FormGroup;

  private _onDestroy = new Subject();

  constructor(
    public dialogRef: MatDialogRef<any>,
    private fb: FormBuilder,
    private _companyService: CompanyService,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: ICompany) {}

    ngOnInit(){
      console.log('data: ', this.data);
      this.formCompany = this.initForm();
    }

    ngOnDestroy(){
      this._onDestroy.next();
      this._onDestroy.complete();
    }

    initForm(): FormGroup {
        return this.fb.group({
            name: [this.data && this.data.name || '', [Validators.required]]
        });
    }

    onNoClick(): void {

      const controls = this.formCompany.controls;
        if (this.formCompany.invalid) {
            Object.keys(controls)
                .forEach(controlName => controls[controlName].markAsTouched());
            return;
        };

        if(this.data && this.data._id){
          this._companyService.updateCompany({
            companyId: this.data._id,
            ...this.formCompany.value
          }).pipe(takeUntil(this._onDestroy)).subscribe((data)=>{
            this._snackBar.open('Success', '', {
              duration: 3000
            });
            this.dialogRef.close();
          }, err=>{
            this._snackBar.open(err, '', {
              duration: 3000
            });
          });
        } else {
          this._companyService.createCompany(this.formCompany.value).pipe(takeUntil(this._onDestroy)).subscribe((data)=>{
            this._snackBar.open('Success', '', {
              duration: 3000
            });
            this.dialogRef.close();
          }, err=>{
            this._snackBar.open(err, '', {
              duration: 3000
            });
          });
        }

        

    }
}
