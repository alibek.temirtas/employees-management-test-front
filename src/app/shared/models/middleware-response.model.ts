export interface IResponseData<T> {
    data: T | null;
    success: boolean;
    errors: any;
  }
  
  export interface IAuthenticationResult {
    accessToken: string | null;
    message: string | null;
  }
  
  export interface IPaginationResult<T> {
    items: T;
    page?: number;
    pageSize?: number;
    totalCount?: number;
    pageIndex?: number;
    hasPreviousPage?: boolean;
    hasNextPage?: boolean;
  }
  
  export interface IChangePassword{
    currentPassword: string;
    newPassword: string;
    passwordConfirm: string;
  }
  
  export interface IPager {
    page: number;
    pageSize: number;
    totalCount?: number;
    totalPages?: number;
  }
  