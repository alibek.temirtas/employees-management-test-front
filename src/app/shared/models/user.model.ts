export interface User {
  readonly _id?: string;
  firstName: string;
  lastName: string;
  middleName: string;
  email: string;
  password: string;
  company:{
    _id: string,
    name: string,
    description: string
  }
  
  companyId: string;
  isActivated: boolean;
  role: 'admin' | 'employee';
}
