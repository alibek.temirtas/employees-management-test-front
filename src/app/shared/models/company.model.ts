export interface ICompany {
    readonly _id?: string;
    name: string;
    description: string;
  }