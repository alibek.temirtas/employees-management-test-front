import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {NgxPaginationModule} from 'ngx-pagination';


import {MaterialsModule} from './materials.module';

@NgModule({
    declarations: [
    ],
    imports: [
        CommonModule,
        FormsModule,
        MaterialsModule,
        ReactiveFormsModule,
        RouterModule,
        NgxPaginationModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialsModule,
        NgxPaginationModule
    ],
    providers: [
    ]
})

export class SharedModule { }
