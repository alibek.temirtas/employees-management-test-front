import { Injectable } from '@angular/core';
import { IResponseData } from '../models';
import {MatSnackBar} from '@angular/material/snack-bar';
import { isArray } from 'util';

@Injectable({
  providedIn: 'root'
})
export class MiddlewareResponseService {
  constructor(private _snackBar: MatSnackBar) {
  }

  handleError(error){
    this._snackBar.open(error, '', {
      duration: 3000
    });
  }

  parseResponse<T>(response: IResponseData<T>): T{
    if (!response.success) {
      if (!isArray(response.errors)){
        throw (response.errors);
      } else{
        response.errors.forEach((item) => {
          throw (item.msg);
        });
      }
    } 
    return response.data;
  }

}
