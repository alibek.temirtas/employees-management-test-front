import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/guards/auth.guard';
import { routesNames } from './shared/enums/routes-names';

const routes: Routes = [
    {
        path: routesNames.AUTH,
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
    },
    {
        path: routesNames.MAIN,
        loadChildren: () => import('./modules/main/main.module').then(m => m.MainModule),
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
