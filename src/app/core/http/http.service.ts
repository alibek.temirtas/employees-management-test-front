import { Injectable, Injector, Optional, InjectionToken, Inject } from '@angular/core';
import { HttpClient, HttpHandler, HttpInterceptor, HttpRequest, HttpEvent } from '@angular/common/http';
import { ErrorHandlerInterceptor, ApiUrlInterceptor, AuthInterceptor } from '../interceptors';
import { Observable } from 'rxjs';


declare module '@angular/common/http/http' {

    // Augment HttpClient with the added configuration methods from HttpService, to allow in-place replacement of
    // HttpClient with HttpService using dependency injection
    export interface HttpClient {

        /**
         * Skips default error handler for this request.
         * @return The new instance.
         */
        skipErrorHandler(): HttpClient;

    }

}

// From @angular/common/http/src/interceptor: allows to chain interceptors
class HttpInterceptorHandler implements HttpHandler {

    constructor(private next: HttpHandler, private interceptor: HttpInterceptor) { }

    handle(request: HttpRequest<any>): Observable<HttpEvent<any>> {

        return this.interceptor.intercept(request, this.next);
    }

}

export const HTTP_DYNAMIC_INTERCEPTORS = new InjectionToken<HttpInterceptor>('HTTP_DYNAMIC_INTERCEPTORS');

@Injectable()
export class HttpService extends HttpClient {

    constructor(private httpHandler: HttpHandler, private injector: Injector,
        @Optional() @Inject(HTTP_DYNAMIC_INTERCEPTORS) private interceptors: HttpInterceptor[] = []) {
        super(httpHandler);

        if (!this.interceptors) {
            this.interceptors = [
                this.injector.get(ApiUrlInterceptor),
                this.injector.get(ErrorHandlerInterceptor),
                this.injector.get(AuthInterceptor)
            ];
        }
    }

    request(method?: any, url?: any, options?: any): any {
        const handler = this.interceptors.reduceRight(
            (next, interceptor) => new HttpInterceptorHandler(next, interceptor),
            this.httpHandler
        );

        return new HttpClient(handler).request(method, url, options);
    }

    skipErrorHandler(): HttpClient {
        return this.removeInterceptor(ErrorHandlerInterceptor);
    }

    private removeInterceptor(interceptorType: any): HttpService {
        return new HttpService(
            this.httpHandler,
            this.injector,
            this.interceptors.filter(i => !(i instanceof interceptorType))
        );
    }

}
