import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Logout } from '../../store/actions';

// import {NotificationService} from '../../shared/components/notification/notification.service';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {

  constructor(private store?: Store<any>) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(error => this.errorHandler(error)));
  }

  private errorHandler(response: HttpErrorResponse): Observable<HttpEvent<any>> {
    console.log('error !!!: ', response);

    if (response.status === 403 || response.status === 401) {
      this.store.dispatch(new Logout({ sendRequest: false }));
    }
    return throwError(response);

  }

}
