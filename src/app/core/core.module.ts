import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { HttpService } from './http/http.service';

import { ErrorHandlerInterceptor, AuthInterceptor, ApiUrlInterceptor } from './interceptors';

@NgModule({
  imports: [CommonModule, HttpClientModule, RouterModule],
  providers: [
    ErrorHandlerInterceptor,
    ApiUrlInterceptor,
    AuthInterceptor,
    {
      provide: HttpClient,
      useClass: HttpService
    }
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        `${parentModule} has already been loaded. Import Core module in the AppModule only.`
      );
    }
  }
}