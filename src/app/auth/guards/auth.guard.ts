import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Store, select } from '@ngrx/store';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {
  authRouteName: string = 'auth';
  constructor(private authService: AuthService, private store: Store<any>, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot) {

    if (!this.isAuthorized()) {
      this.handleUnauthorized();
      return false;
    }
    return true;
  }

  private isAuthorized() {
    return localStorage.getItem('access_token') && localStorage.getItem('user');
  }

  private handleUnauthorized() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('user');
    this.router.navigateByUrl(this.authRouteName, { state: { skipRequest: true } });
  }
}
