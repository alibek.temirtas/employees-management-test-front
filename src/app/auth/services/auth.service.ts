import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthData } from '../models/auth-data.model';
import { Observable } from 'rxjs';
import { User } from '../../shared/models';
import {map} from 'rxjs/operators';

import {IPaginationResult, IResponseData, IAuthenticationResult} from '../../shared/models'

import {MiddlewareResponseService} from '../../shared/services/middleware-response.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(private http: HttpClient, private _middleWareResponseSerivce: MiddlewareResponseService) { }

    login(authData: AuthData) {
        return this.http.post(`auth/login`, authData).pipe(
            map((response: IResponseData<IAuthenticationResult>) => {
              return this._middleWareResponseSerivce.parseResponse<IAuthenticationResult>(response);
            })
          );
    }

    getUserDetails(): Observable<User> {
        return this.http.get<User>(`auth/user/info`).pipe(
            map((response: any) => {
              return this._middleWareResponseSerivce.parseResponse<any>(response);
            })
          );
    }

}
