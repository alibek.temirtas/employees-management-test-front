import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthData } from '../models/auth-data.model';
import { Store, select } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { selectLoginError } from '../../store/selectors';
import { Login } from '../../store/actions/user.actions';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {

    formLogin: FormGroup;
    loginErrorMsg: string;
    subscription: Subscription;

    constructor(private fb: FormBuilder, private store: Store<any>) { }

    ngOnInit() {
        this.formLogin = this.initForm();
        this.subscription = this.store.pipe(select(selectLoginError)).subscribe((loginErrorMsg) => {
            this.loginErrorMsg = loginErrorMsg;
        });
    }

    initForm(): FormGroup {
        return this.fb.group({
            email: ['', [Validators.email, Validators.required]],
            password: ['', Validators.required]
        });
    }

    onSubmit() {
        const controls = this.formLogin.controls;
        if (this.formLogin.invalid) {
            Object.keys(controls)
                .forEach(controlName => controls[controlName].markAsTouched());
            return;
        };

        
        this.store.dispatch(new Login({authData: this.formLogin.value}));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
